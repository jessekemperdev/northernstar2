<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package northern-star
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<header id="masthead" class="site-header">
		<div class="container">
		<div class="site-branding">
	<a href="/">
			<svg width="500" height="99" viewBox="0 0 500 99" fill="none" xmlns="http://www.w3.org/2000/svg">
			<mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="54" width="40" height="45">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M0.811523 54.1447H39.7802V99H0.811523V54.1447Z" fill="white"/>
			</mask>
			<g mask="url(#mask0)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M27.195 99V74.2332C27.195 68.569 25.2239 65.926 20.5991 65.926C15.5197 65.926 13.3968 68.7202 13.3968 74.384V99H0.811523V54.1985H12.5629V60.0355H12.7146C15.3679 55.7307 19.083 54.1447 24.1621 54.1447C31.744 54.1447 39.7803 58.3741 39.7803 70.7591V99H27.195Z" fill="#FEFEFE"/>
			</g>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M79.3197 76.4998C79.3197 70.8558 75.1058 65.8219 69.2364 65.8219C63.7431 65.8219 59.3033 70.7031 59.3033 76.4998C59.3033 82.2965 63.6676 87.1781 69.3115 87.1781C75.181 87.1781 79.3197 82.1438 79.3197 76.4998ZM69.3115 99C56.5939 99 46.8115 89.4661 46.8115 76.4998C46.8115 63.3812 56.8197 54 69.3115 54C81.8785 54 91.8115 63.5335 91.8115 76.3471C91.8115 89.6185 81.8785 99 69.3115 99Z" fill="#FEFEFE"/>
			<mask id="mask1" mask-type="alpha" maskUnits="userSpaceOnUse" x="100" y="54" width="23" height="45">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M100.013 54.1447H122.777V99H100.013V54.1447Z" fill="white"/>
			</mask>
			<g mask="url(#mask1)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M100.013 99V54.1985H111.97V60.0355H112.123C114.346 55.5795 117.872 54.1447 122.777 54.1447V66.7567C115.189 66.8321 112.736 69.6267 112.736 74.384V99H100.013Z" fill="#FEFEFE"/>
			</g>
			<mask id="mask2" mask-type="alpha" maskUnits="userSpaceOnUse" x="129" y="41" width="26" height="58">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M129.872 41.236H154.713V99H129.872V41.236Z" fill="white"/>
			</mask>
			<g mask="url(#mask2)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M136.082 99.0001V63.4848H129.872V53.9077H136.082V41.236H148.654V53.9077H154.713V63.4848H148.654V99.0001H136.082Z" fill="#FEFEFE"/>
			</g>
			<mask id="mask3" mask-type="alpha" maskUnits="userSpaceOnUse" x="163" y="41" width="39" height="58">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M163.055 41.236H201.702V99H163.055V41.236Z" fill="white"/>
			</mask>
			<g mask="url(#mask3)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M189.221 99.0001V74.3005C189.221 68.8277 187.115 65.7113 182.529 65.7113C177.792 65.7113 175.536 68.5237 175.536 73.7684V99.0001H163.055V41.236H175.536V59.3264C177.341 55.9061 181.401 53.8536 186.364 53.8536C195.912 53.8536 201.702 59.9344 201.702 70.9561V99.0001H189.221Z" fill="#FEFEFE"/>
			</g>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M240.305 71.8634C239.325 67.5305 235.558 64.6418 230.736 64.6418C226.215 64.6418 222.298 67.3783 221.318 71.8634H240.305ZM221.167 80.3767C221.846 85.1658 225.839 88.3579 230.962 88.3579C234.352 88.3579 236.537 87.0659 238.572 84.4053H251.38C247.387 93.6032 239.777 99 230.962 99C218.907 99 208.812 89.1944 208.812 76.6518C208.812 64.4899 218.455 54 230.736 54C243.243 54 252.812 63.9578 252.812 77.0318C252.812 78.2482 252.736 79.2362 252.51 80.3767H221.167Z" fill="#FEFEFE"/>
			<mask id="mask4" mask-type="alpha" maskUnits="userSpaceOnUse" x="262" y="54" width="23" height="45">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M262.083 54.1447H284.527V99H262.083V54.1447Z" fill="white"/>
			</mask>
			<g mask="url(#mask4)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M262.083 99V54.1985H273.872V60.0355H274.023C276.215 55.5795 279.69 54.1447 284.527 54.1447V66.7567C277.046 66.8321 274.628 69.6267 274.628 74.384V99H262.083Z" fill="#FEFEFE"/>
			</g>
			<mask id="mask5" mask-type="alpha" maskUnits="userSpaceOnUse" x="292" y="54" width="39" height="45">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M292.041 54.1447H330.687V99H292.041V54.1447Z" fill="white"/>
			</mask>
			<g mask="url(#mask5)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M318.206 99V74.2332C318.206 68.569 316.252 65.926 311.665 65.926C306.627 65.926 304.522 68.7202 304.522 74.384V99H292.041V54.1985H303.695V60.0355H303.845C306.477 55.7307 310.161 54.1447 315.199 54.1447C322.718 54.1447 330.687 58.3741 330.687 70.7591V99H318.206Z" fill="#FEFEFE"/>
			</g>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M345.722 5L346.943 13.6436L354.812 17.5456L346.904 21.3692L345.596 30L339.488 23.7196L330.812 25.1517L334.944 17.4468L330.89 9.70084L339.552 11.2193L345.722 5Z" stroke="#FEFEFE" stroke-width="4"/>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M373.271 67.28C373.196 64.8517 371.466 64.0928 369.435 64.0928C367.329 64.0928 365.825 65.3071 365.825 66.9007C365.825 69.177 367.856 70.3155 373.648 71.8329C383.578 74.4894 386.812 78.5113 386.812 84.6577C386.812 93.46 379.289 99 369.812 99C360.409 99 353.714 93.6121 352.812 85.0367H365.223C365.599 87.6932 367.48 88.9072 369.962 88.9072C372.143 88.9072 374.324 87.617 374.324 85.6442C374.324 83.2917 372.896 82.0774 366.652 80.1049C356.12 76.8415 353.337 72.6676 353.337 67.7354C353.337 59.3879 361.085 54 369.51 54C378.537 54 384.931 59.0086 385.533 67.28H373.271Z" fill="#C4D82D"/>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M398.017 99L398.061 63.2198H391.812V53.5982H398.061L398.017 41H410.669L410.714 53.5982H416.812V63.2198H410.714L410.669 99H398.017Z" fill="#C4D82D"/>
			<path fill-rule="evenodd" clip-rule="evenodd" d="M455.273 76.4511C455.273 70.5459 450.514 65.7766 444.623 65.7766C438.882 65.7766 434.351 70.5459 434.351 76.6028C434.351 82.4323 438.958 87.2017 444.774 87.2017C450.665 87.2017 455.273 82.508 455.273 76.4511ZM455.273 99V92.9558H455.122C453.309 96.6653 448.324 98.9367 442.81 98.9367C430.422 98.9367 421.812 89.1703 421.812 76.4511C421.812 64.035 430.8 54.0412 442.81 54.0412C447.947 54.0412 452.705 56.0859 455.122 59.9471H455.273V54H467.812V99H455.273Z" fill="#C4D82D"/>
			<mask id="mask6" mask-type="alpha" maskUnits="userSpaceOnUse" x="477" y="54" width="23" height="45">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M477.038 54.1163H499.784V99H477.038V54.1163Z" fill="white"/>
			</mask>
			<g mask="url(#mask6)">
			<path fill-rule="evenodd" clip-rule="evenodd" d="M477.038 99V54.1163H488.985V60.088H489.139C491.359 55.6353 494.883 54.2014 499.784 54.2014V66.8034C492.202 66.8791 489.751 69.6714 489.751 74.4252V99H477.038Z" fill="#C4D82D"/>
			</g>
			</svg>
</a>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'northern-star' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);

			?>


		</nav><!-- #site-navigation -->


	<?php if (is_single()){
		?>
		<h2 class="entry-title">NEWS ARTICLE</h2>
		<?php
	}
	else if (is_home()){
		echo "";
	}
	else{
		?>
		<h2 class="entry-title">
			<?php
		the_title();?>
	</h2>
		<?php
	}
	?>
</div>
	</header><!-- #masthead -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'northern-star' ); ?></a>
