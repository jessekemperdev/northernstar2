<?php
/* Template Name: Team Page */
get_header();

 ?>
 <div class="container-inner">
 <?php
 $args = array(
   'numberposts' => -1,
   'post_type'   => 'team',
   'tax_query' => array(
        array(
            'taxonomy' => 'team_categories',
            'terms' => 2,
            'field' => 'term_id',
        ))
 );

 $management = get_posts( $args );

  ?>
<h2 class="team-title">Management</h2>
<div class="team-list">
  <?php foreach ($management as $member){

    $title = get_field( "position__title", $member->ID);
    $image = get_the_post_thumbnail_url($member->ID, 'full');

    ?>
    <div class="team-member" data-micromodal-trigger="modal-<?php echo $member->ID;?>">
    <?php if($image){
    ?>
    <div class="team-image" style="background:url('<?php echo $image;?>') center no-repeat;'"></div>
  <?php
    }
    else{
      ?>
      <div class="team-image" style="background:url('/wp-content/uploads/2021/01/creative-vector-illustration-default-avatar-profile-placeholder-isolated-background-art-design-grey-photo-blank-template-mo-118822720.jpg') center no-repeat;" /></div>
      <?php
    }
  ?>

    <h2 class="entry-title"><?php echo $member->post_title; ?></h2>
    <h3 class="team-position"><?php echo $title; ?></h3>
  </div>

  <!-- [1] -->
<div class="modal" id="modal-<?php echo $member->ID;?>">

<button class="close-modal">X</button>



      <div id="modal-1-content">
        <?php if($image){
        ?>
        <div class="team-image" style="background:url('<?php echo $image;?>') center no-repeat;'"></div>
      <?php
        }
        else{
          ?>
          <div class="team-image" style="background:url('/wp-content/uploads/2021/01/creative-vector-illustration-default-avatar-profile-placeholder-isolated-background-art-design-grey-photo-blank-template-mo-118822720.jpg') center no-repeat;" /></div>
          <?php
        }
      ?>
        <h2 class="entry-title"><?php echo $member->post_title; ?></h2>
        <h3 class="team-position"><?php echo $title; ?></h3>
        <?php echo $member->post_content;?>
      </div>

    </div>


    <?php
  }?>

</div>

<?php
$args = array(
  'numberposts' => -1,
  'post_type'   => 'team',
  'tax_query' => array(
       array(
           'taxonomy' => 'team_categories',
           'terms' => 3,
           'field' => 'term_id',
       ))
);
$board = get_posts($args);
 ?>

<h2 class="team-title">Board Members</h2>
<div class="team-list">
  <?php foreach ($board as $member){

    $title = get_field( "position__title", $member->ID);
    $image = get_the_post_thumbnail_url($member->ID, 'full');

    ?>
    <div class="team-member"  data-micromodal-trigger="modal-<?php echo $member->ID;?>">
    <?php if($image){
    ?>
    <div class="team-image" style="background:url('<?php echo $image;?>') center no-repeat;'"></div>
  <?php
    }
    else{
      ?>
      <div class="team-image" style="background:url('/wp-content/uploads/2021/01/creative-vector-illustration-default-avatar-profile-placeholder-isolated-background-art-design-grey-photo-blank-template-mo-118822720.jpg') center no-repeat;" /></div>
      <?php
    }
  ?>

    <h2 class="entry-title"><?php echo $member->post_title; ?></h2>
    <h3 class="team-position"><?php echo $title; ?></h3>
  </div>
  <!-- [1] -->
<div class="modal" id="modal-<?php echo $member->ID;?>">

<button class="close-modal">X</button>



      <div id="modal-1-content">
        <?php if($image){
        ?>
        <div class="team-image" style="background:url('<?php echo $image;?>') center no-repeat;'"></div>
      <?php
        }
        else{
          ?>
          <div class="team-image" style="background:url('/wp-content/uploads/2021/01/creative-vector-illustration-default-avatar-profile-placeholder-isolated-background-art-design-grey-photo-blank-template-mo-118822720.jpg') center no-repeat;" /></div>
          <?php
        }
      ?>
        <h2 class="entry-title"><?php echo $member->post_title; ?></h2>
        <h3 class="team-position"><?php echo $title; ?></h3>
        <?php echo $member->post_content;?>
      </div>

    </div>
    <?php
  }?>

</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function(){

  $('.team-member').click(function(){
    let trigger = $(this).attr("data-micromodal-trigger");
    $('#'+trigger).fadeIn();
  });
});
$('.close-modal').click(function(){
  $(".modal").fadeOut();
});
</script>
 <?php
 get_footer();
?>
