<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package northern-star
 */

?>


</div><!-- #page -->

<?php wp_footer(); ?>
<footer id="colophon" class="site-footer">

	<div class="site-info">
		<div class="ticker"><svg width="36" height="47" viewBox="0 0 36 47" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect width="36" height="47" fill="url(#pattern0)"/>
<defs>
<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image0" transform="scale(0.0126582 0.00952381)"/>
</pattern>
<image id="image0" width="79" height="105" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE8AAABpCAYAAAB7wX6tAAAAAXNSR0IArs4c6QAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAT6ADAAQAAAABAAAAaQAAAABMAjwQAAAFBklEQVR4Ae2a25ITMQxEWYr//2XY3qULoUi2bMvOTCI/4KtarTPOpbJ8/Pjbfn82jmf6j882ExfJO6st/UTy4PxIrl8ywcpYmxsxsZLXi9V+vHN6Xcb1akiD55noGdBxq3NZfJaWV8PP1QS9+MxinpULNaDp/NvhIaGVWBtZnT8jxxF4uwGeAGc93GPwrOQZa88CB+9DHxjWG+eIeZy1NGYhjuRmDi9/REvHDsGjAdlTMJJcxp0e06eXV+5btch9aqS9bC1xJtnRWwV6eUa96fN6zjxp8Ch4td4rvOeTceyt86nwWoms5LvXVv304lPh7YZxNf1UeCPvQysgInl6t2YlP2NT4VH0Xfo0eJHb8GpQh77nrQI68VI6+YDSbt5J01fJdQzeq906PMBj8K5yWzJ9DL3nzSR+xRtHDltv3i5wEd3VDzcCavVbbl6kuJapq+zxAXj1DMHzRK5SrPaB4mc9Exw0PZ2tL1tdTOY8CkVCiOa3Yqy128KLgsA5q3AvvnVW790aXvT2ESCKR7PAfe/Ye/K8jB96z5Midx5LACt13PrmofCR27cCirEy3+3hnQQowSHvS8AjQF0c1rOapf0y8AjJKpJ7s72n+ZIfGCx29YOBOh70qf+Q6Ildfb0Hswfr6vWVvyJQBIpAESgCRaAICAJf3/N633/E+RoWgSJQBIpAESgCRaAI3IPAkd/z5PfIV/rN7D94o0XyvAWEe607xLjIWakzE8cY6ETzyRiZn+Mtf8PQ5mACjUnZ63Ncv0rf87f1bxgaGOc0xTl7CU2fkXt6bMXrM9bci2NuxGDsnUu/eTKxZRhrMIPm7T97/dvdP39eTenwnl34yfzp8OSN8p7YyQJXcslaLJ10eDoJAKLp9bvNrRq2fGDgielkct57oqNgpbYVm52PObbdPBhGYyLZo1g0uXbH8ZabJ0FIgBoY5nJfxo2MMzRG8vHstpvHBLJHkWhy7S5jy/dReARlGeHelXr9StHengJPm7jrfMt7Hp+Yd8O4f1Vo2p9XhwtPC8hCPTGckXEc8zzn1OI657O91rV0rFyzcdRPf9l6JiNGaeoqvVWL9Lb9k09D6xmS5mpcBIpAESgCRaAIFIEiME3g4UfLaaU3C6zvq2/2wKvcIlAEikARKAJF4K0IPHxJHv3yp3/sBD1qWHty3yMdjfPOUZc+OJd9L1aetcbQfvgZflXUSqTXWjm8PQkCZ9C0rp5/n/r+V+9lzB/gQTRiLJJcFhw5Hzkz6202ruXJhIeAHcmkEUvfWkNMxkPI0JD+MXbhYdMrBnvRlmm65yczV6Q+9++2DIbhXaaktgcmkluekWNqyjXW5fUjZ5s3jwlgAo3z0X7E0Kh26zzyorXOrOyF4DHBLoCebrRwxKPR56m++7LVRmAyWpSOteaZRUutWY9Sw/KLNWoP3TyKRRLwrOyZVK5545GzlgY8oll7WWtT8JB8tzGrwBmgXwQ3QXThRYzOAIzots609izgXJvxylivd+EhYNaolyxrHb7QsvRmdbofGDSZ+eSg6ekxX6QgfdbTjGjxjNbkutU3b54MGBGVcSfH8Ijm5cyAK7XD8BDUMiZFd497EE75HIJ3BYAEhx7NelDeunV2Za37nmeJ48meMmjll2sjPiI3MqoHreGbR+MRIzyb1UcLy8rX05mGB+HTAFfyrcR6EJfg3QEgoKF5AFbWt4iuGJqJlS/nXaAsX38Af+VY7KpFWzgAAAAASUVORK5CYII="/>
</defs>
</svg>
</div>
		<p>For more information, please contact us at info@northernstaric2.com. Privacy Policy</p>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</body>
</html>
