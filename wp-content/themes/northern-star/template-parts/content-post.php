<div class="news-block">
  <div class="date-row">
    <span class="blog-news-date"><?php echo get_the_date('d');?>
    </span>
    <span class="blog-news-month">
      <?php echo get_the_date('M');?></div>
    </span>
  <div class="news-text">
<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
</div>
</div>
<div class="single-news-post">
<?php the_content(); ?>

</div>
