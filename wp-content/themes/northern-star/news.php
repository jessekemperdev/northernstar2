<?php
/*
Template Name: News

*/
get_header();

 ?>

 <div class="news-posts">
 <?php
 $lastposts = get_posts( array(
     'posts_per_page' => 100
 ) );

 if ( $lastposts ) {
     foreach ( $lastposts as $post ) :
         setup_postdata( $post ); ?>
         <div class="news-block">
           <div class="date-row">
             <span class="blog-news-date"><?php echo get_the_date('d');?>
             </span>
             <span class="blog-news-month">
               <?php echo get_the_date('M');?></div>
             </span>
           <div class="news-text">
         <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
       </div>
       </div>
     <?php
     endforeach;
     wp_reset_postdata();
 }
 ?>
</div>
<?php
get_footer();
 ?>
